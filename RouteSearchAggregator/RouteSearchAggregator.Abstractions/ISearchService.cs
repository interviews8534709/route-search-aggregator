﻿namespace RouteSearchAggregator.Abstractions;

public interface ISearchService
{
    public Task<SearchResponse> SearchAsync(SearchRequest request, CancellationToken cancellationToken);

    public Task<bool> IsAvailableAsync(CancellationToken cancellationToken);
}