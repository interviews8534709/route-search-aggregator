﻿using RouteSearchAggregator.Abstractions;
using RouteSearchAggregator.Implementations.Adapters;

namespace RouteSearchAggregator.Implementations;

public class SearchService : ISearchService
{
    private readonly ISearchService[] externalProviderAdapters;

    public SearchService(IProviderOneAdapter providerOneAdapter, IProviderTwoAdapter providerTwoAdapter)
    {
        externalProviderAdapters = new ISearchService[]
        {
            providerOneAdapter,
            providerTwoAdapter
        };
    }
    
    public async Task<SearchResponse> SearchAsync(SearchRequest request, CancellationToken cancellationToken)
    {
        List<Task<SearchResponse>> searchTasks = new List<Task<SearchResponse>>();

        foreach (var externalProviderAdapter in externalProviderAdapters)
        {
            bool isExternalProviderAvailable = await externalProviderAdapter.IsAvailableAsync(cancellationToken);
            if (isExternalProviderAvailable)
            {
                searchTasks.Add(externalProviderAdapter.SearchAsync(request, cancellationToken));
            }
        }

        var mergedRoutes = new List<Route>();

        decimal minPrice = decimal.MaxValue;
        decimal maxPrice = 0;
        int minMinutesRoute = int.MaxValue;
        int maxMinutesRoute = 0;

        foreach (var response in searchTasks.Select(task => task.Result))
        {
            mergedRoutes.AddRange(response.Routes);
            minPrice = Math.Min(minPrice, response.MinPrice);
            maxPrice = Math.Max(maxPrice, response.MaxPrice);
            minMinutesRoute = Math.Min(minMinutesRoute, response.MinMinutesRoute);
            maxMinutesRoute = Math.Max(maxMinutesRoute, response.MaxMinutesRoute);
        }

        return new SearchResponse
        {
            Routes = mergedRoutes.ToArray(),
            MinPrice = minPrice,
            MaxPrice = maxPrice,
            MinMinutesRoute = minMinutesRoute,
            MaxMinutesRoute = maxMinutesRoute
        };
    }

    public Task<bool> IsAvailableAsync(CancellationToken cancellationToken)
    {
        return Task.FromResult(true);
    }
}