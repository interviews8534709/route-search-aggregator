﻿using System.Text;
using Newtonsoft.Json;
using RouteSearchAggregator.Abstractions;

namespace RouteSearchAggregator.Implementations.Adapters;

/// <summary>
/// Adapter to call ProviderOne API
/// </summary>
public class ProviderOneAdapter : IProviderOneAdapter
{
    private readonly IHttpClientFactory _httpClientFactory;

    public ProviderOneAdapter(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = httpClientFactory;
    }

    public async Task<SearchResponse> SearchAsync(SearchRequest request, CancellationToken cancellationToken)
    {
        var providerOneRequest = CreateProviderOneSearchRequest(request);

        var client = _httpClientFactory.CreateClient("ProviderOne");
        var content = new StringContent(JsonConvert.SerializeObject(providerOneRequest), Encoding.UTF8, "application/json");

        var response = await client.PostAsync("api/v1/search", content, cancellationToken);
        response.EnsureSuccessStatusCode();

        var providerOneResponse = JsonConvert.DeserializeObject<ProviderOneSearchResponse>(
            await response.Content.ReadAsStringAsync(cancellationToken));
        
        var searchResponse = CreateSearchResponse(providerOneResponse);

        return searchResponse;
    }

    private static SearchResponse CreateSearchResponse(ProviderOneSearchResponse? providerOneResponse)
    {
        var routes = providerOneResponse.Routes.Select(r => new Route
        {
            Id = Guid.NewGuid(),
            Origin = r.From,
            Destination = r.To,
            OriginDateTime = r.DateFrom,
            DestinationDateTime = r.DateTo,
            Price = r.Price,
            TimeLimit = r.TimeLimit
        }).ToList();

        var searchResponse = new SearchResponse
        {
            Routes = routes.ToArray(),
            MinPrice = routes.Min(r => r.Price),
            MaxPrice = routes.Max(r => r.Price),
            MinMinutesRoute = (int)routes.Min(r => (r.DestinationDateTime - r.OriginDateTime).TotalMinutes),
            MaxMinutesRoute = (int)routes.Max(r => (r.DestinationDateTime - r.OriginDateTime).TotalMinutes)
        };
        return searchResponse;
    }

    private static ProviderOneSearchRequest CreateProviderOneSearchRequest(SearchRequest request)
    {
        var providerOneRequest = new ProviderOneSearchRequest
        {
            From = request.Origin,
            To = request.Destination,
            DateFrom = request.OriginDateTime,
            DateTo = request.Filters?.DestinationDateTime,
            MaxPrice = request.Filters?.MaxPrice
        };
        return providerOneRequest;
    }

    public async Task<bool> IsAvailableAsync(CancellationToken cancellationToken)
    {
        var client = _httpClientFactory.CreateClient("ProviderOne");
        try
        {
            var response = await client.GetAsync("api/v1/ping", cancellationToken);
            return response.IsSuccessStatusCode;
        }
        catch (HttpRequestException)
        {
            return false;
        }
    }

    #region External model

    internal class ProviderOneSearchRequest
    {
        // Mandatory
        // Start point of route, e.g. Moscow 
        public string From { get; set; }
    
        // Mandatory
        // End point of route, e.g. Sochi
        public string To { get; set; }
    
        // Mandatory
        // Start date of route
        public DateTime DateFrom { get; set; }
    
        // Optional
        // End date of route
        public DateTime? DateTo { get; set; }
    
        // Optional
        // Maximum price of route
        public decimal? MaxPrice { get; set; }
    }

    internal class ProviderOneSearchResponse
    {
        // Mandatory
        // Array of routes
        public ProviderOneRoute[] Routes { get; set; }
    }

    internal class ProviderOneRoute
    {
        // Mandatory
        // Start point of route
        public string From { get; set; }
    
        // Mandatory
        // End point of route
        public string To { get; set; }
    
        // Mandatory
        // Start date of route
        public DateTime DateFrom { get; set; }
    
        // Mandatory
        // End date of route
        public DateTime DateTo { get; set; }
    
        // Mandatory
        // Price of route
        public decimal Price { get; set; }
    
        // Mandatory
        // Timelimit. After it expires, route became not actual
        public DateTime TimeLimit { get; set; }
    }

    #endregion
}

public interface IProviderOneAdapter : ISearchService
{
    Task<SearchResponse> SearchAsync(SearchRequest request, CancellationToken cancellationToken);
    Task<bool> IsAvailableAsync(CancellationToken cancellationToken);
}