﻿using System.Text;
using Newtonsoft.Json;
using RouteSearchAggregator.Abstractions;

namespace RouteSearchAggregator.Implementations.Adapters;

/// <summary>
/// Adapter to call ProviderTwo API
/// </summary>
public class ProviderTwoAdapter : IProviderTwoAdapter
{
    private readonly IHttpClientFactory _httpClientFactory;

    public ProviderTwoAdapter(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = httpClientFactory;
    }
    
    public async Task<SearchResponse> SearchAsync(SearchRequest request, CancellationToken cancellationToken)
    {
        var providerTwoRequest = CreateProviderTwoSearchRequest(request);

        var client = _httpClientFactory.CreateClient("ProviderTwo");
        var content = new StringContent(JsonConvert.SerializeObject(providerTwoRequest), Encoding.UTF8, "application/json");

        var response = await client.PostAsync("api/v1/search", content, cancellationToken);
        response.EnsureSuccessStatusCode();

        var searchResponse = await CreateSearchResponse(response);

        return searchResponse;
    }

    private static async Task<SearchResponse> CreateSearchResponse(HttpResponseMessage response)
    {
        var providerTwoResponse =
            JsonConvert.DeserializeObject<ProviderTwoSearchResponse>(await response.Content.ReadAsStringAsync());
        var routes = providerTwoResponse.Routes.Select(r => new Route
        {
            Id = Guid.NewGuid(),
            Origin = r.Departure.Point,
            Destination = r.Arrival.Point,
            OriginDateTime = r.Departure.Date,
            DestinationDateTime = r.Arrival.Date,
            Price = r.Price,
            TimeLimit = r.TimeLimit
        }).ToList();

        var searchResponse = new SearchResponse
        {
            Routes = routes.ToArray(),
            MinPrice = routes.Min(r => r.Price),
            MaxPrice = routes.Max(r => r.Price),
            MinMinutesRoute = (int)routes.Min(r => (r.DestinationDateTime - r.OriginDateTime).TotalMinutes),
            MaxMinutesRoute = (int)routes.Max(r => (r.DestinationDateTime - r.OriginDateTime).TotalMinutes)
        };
        return searchResponse;
    }

    private static ProviderTwoSearchRequest CreateProviderTwoSearchRequest(SearchRequest request)
    {
        var providerTwoRequest = new ProviderTwoSearchRequest
        {
            Departure = request.Origin,
            Arrival = request.Destination,
            DepartureDate = request.OriginDateTime,
            MinTimeLimit = request.Filters?.DestinationDateTime
        };
        return providerTwoRequest;
    }

    public async Task<bool> IsAvailableAsync(CancellationToken cancellationToken)
    {
        var client = _httpClientFactory.CreateClient("ProviderTwo");
        try
        {
            var response = await client.GetAsync($"api/v1/ping", cancellationToken);
            return response.IsSuccessStatusCode;
        }
        catch (HttpRequestException)
        {
            return false;
        }
    }

    private sealed class ProviderTwoSearchRequest
    {
        // Mandatory
        // Start point of route, e.g. Moscow 
        public string Departure { get; set; }
    
        // Mandatory
        // End point of route, e.g. Sochi
        public string Arrival { get; set; }
    
        // Mandatory
        // Start date of route
        public DateTime DepartureDate { get; set; }
    
        // Optional
        // Minimum value of timelimit for route
        public DateTime? MinTimeLimit { get; set; }
    }

    internal class ProviderTwoSearchResponse
    {
        // Mandatory
        // Array of routes
        public ProviderTwoRoute[] Routes { get; set; }
    }

    internal class ProviderTwoRoute
    {
        // Mandatory
        // Start point of route
        public ProviderTwoPoint Departure { get; set; }
    
    
        // Mandatory
        // End point of route
        public ProviderTwoPoint Arrival { get; set; }
    
        // Mandatory
        // Price of route
        public decimal Price { get; set; }
    
        // Mandatory
        // Timelimit. After it expires, route became not actual
        public DateTime TimeLimit { get; set; }
    }

    internal class ProviderTwoPoint
    {
        // Mandatory
        // Name of point, e.g. Moscow\Sochi
        public string Point { get; set; }
    
        // Mandatory
        // Date for point in Route, e.g. Point = Moscow, Date = 2023-01-01 15-00-00
        public DateTime Date {get; set; }
    }
}

public interface IProviderTwoAdapter : ISearchService
{
    Task<SearchResponse> SearchAsync(SearchRequest request, CancellationToken cancellationToken);
    Task<bool> IsAvailableAsync(CancellationToken cancellationToken);
}