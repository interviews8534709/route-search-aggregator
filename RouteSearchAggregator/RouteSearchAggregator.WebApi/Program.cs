using RouteSearchAggregator.WebApi;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


// Register business services
builder.Services.AddHttpClient(
    "ProviderOne", client => { client.BaseAddress = new Uri(builder.Configuration["ProviderOneBaseUrl"]); });
builder.Services.AddHttpClient(
    "ProviderTwo", client => { client.BaseAddress = new Uri(builder.Configuration["ProviderTwoBaseUrl"]); });
builder.Services.RegisterServices();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();