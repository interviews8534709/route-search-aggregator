﻿using RouteSearchAggregator.Abstractions;
using RouteSearchAggregator.Implementations;
using RouteSearchAggregator.Implementations.Adapters;

namespace RouteSearchAggregator.WebApi;

public static class ServicesExtensions
{
    public static void RegisterServices(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<ISearchService, SearchService>();
        serviceCollection.AddScoped<IProviderOneAdapter, ProviderOneAdapter>();
        serviceCollection.AddScoped<IProviderTwoAdapter, ProviderTwoAdapter>();
    }
}