﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using RouteSearchAggregator.Abstractions;

namespace RouteSearchAggregator.WebApi.Controllers;

[ApiController]
[Route("routes")]
public class RoutesController : Controller
{
    private readonly ISearchService _searchService;

    public RoutesController(ISearchService searchService)
    {
        _searchService = searchService;
    }

    /// <summary>
    /// Search routes.
    /// </summary>
    /// <param name="searchRequest">Request to search routes.</param>
    /// <param name="cancellationToken">Token to cancel operations.</param>
    [HttpPost]
    public async Task<IActionResult> SearchRoutes(SearchRequest searchRequest, CancellationToken cancellationToken)
    {
        // TODO 1 - реализовать агрегацию данных от обоих источников
            // TODO 1.2 - видится, что нужно использовать Wait.ALL и вызывать обе API-шки
        // TODO 2 - реализовать кеширование
        // TODO 3 - использовать cancellationToken для отмены операции
        
        SearchResponse response = await _searchService.SearchAsync(searchRequest, cancellationToken);

        return Ok(response);
    }
    
    /// <summary>
    /// Check service is available.
    /// </summary>
    /// <response code="200"> If service available.</response>
    [HttpGet]
    [Route("api/v1/ping")]
    public IActionResult Ping()
    {
        return Ok();
    }
}