using Microsoft.AspNetCore.Mvc;
using MockProviders.MobileApi.Controllers.Dto;

namespace MockProviders.MobileApi.Controllers;

/// <summary>
/// Mock for ProviderTwo.
/// </summary>
[ApiController]
[Route("[controller]")]
public class ProviderTwoController : ControllerBase
{
    [HttpGet]
    [Route("api/v1/ping")]
    public IActionResult Ping()
    {
        return Ok();
    }

    [HttpPost]
    [Route("api/v1/search")]
    public IActionResult Search([FromBody] ProviderTwoSearchRequest request)
    {
        var response = new ProviderTwoSearchResponse
        {
            Routes = new[]
            {
                new ProviderTwoRoute {
                    Departure = new ProviderTwoPoint { Point = "Moscow", Date = DateTime.Now },
                    Arrival = new ProviderTwoPoint { Point = "St. Petersburg", Date = DateTime.Now.AddHours(5) },
                    Price = 100,
                    TimeLimit = DateTime.Now.AddDays(7)
                },
                new ProviderTwoRoute {
                    Departure = new ProviderTwoPoint { Point = "New York", Date = DateTime.Now },
                    Arrival = new ProviderTwoPoint { Point = "Los Angeles", Date = DateTime.Now.AddHours(6) },
                    Price = 200,
                    TimeLimit = DateTime.Now.AddDays(14)
                },
                new ProviderTwoRoute {
                    Departure = new ProviderTwoPoint { Point = "London", Date = DateTime.Now },
                    Arrival = new ProviderTwoPoint { Point = "Paris", Date = DateTime.Now.AddHours(3) },
                    Price = 150,
                    TimeLimit = DateTime.Now.AddDays(10)
                },
                new ProviderTwoRoute {
                    Departure = new ProviderTwoPoint { Point = "Tokyo", Date = DateTime.Now },
                    Arrival = new ProviderTwoPoint { Point = "Sydney", Date = DateTime.Now.AddHours(9) },
                    Price = 300,
                    TimeLimit = DateTime.Now.AddDays(21)
                },
                new ProviderTwoRoute {
                    Departure = new ProviderTwoPoint { Point = "Dubai", Date = DateTime.Now },
                    Arrival = new ProviderTwoPoint { Point = "Singapore", Date = DateTime.Now.AddHours(7) },
                    Price = 250,
                    TimeLimit = DateTime.Now.AddDays(14)
                }
            }
        };

        return Ok(response);
    }
}