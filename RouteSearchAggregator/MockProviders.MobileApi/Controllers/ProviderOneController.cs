using Microsoft.AspNetCore.Mvc;
using MockProviders.MobileApi.Controllers.Dto;

namespace MockProviders.MobileApi.Controllers;

/// <summary>
/// Mock for ProviderTwo.
/// </summary>
[ApiController]
[Route("[controller]")]
public class ProviderOneController : ControllerBase
{
    [HttpGet]
    [Route("api/v1/ping")]
    public IActionResult Ping()
    {
        return Ok();
    }

    [HttpPost]
    [Route("api/v1/search")]
    public IActionResult Search([FromBody] ProviderOneSearchRequest request)
    {
        var response = new ProviderOneSearchResponse
        {
            Routes = new[]
            {
                new ProviderOneRoute
                {
                    From = "Moscow",
                    To = "Sochi",
                    DateFrom = DateTime.Today.AddDays(10),
                    DateTo = DateTime.Today.AddDays(12),
                    Price = 1000,
                    TimeLimit = DateTime.Today.AddDays(5)
                },
                new ProviderOneRoute
                {
                    From = "St. Petersburg",
                    To = "Kazan",
                    DateFrom = DateTime.Today.AddDays(7),
                    DateTo = DateTime.Today.AddDays(9),
                    Price = 800,
                    TimeLimit = DateTime.Today.AddDays(3)
                },
                new ProviderOneRoute
                {
                    From = "Novosibirsk",
                    To = "Vladivostok",
                    DateFrom = DateTime.Today.AddDays(15),
                    DateTo = DateTime.Today.AddDays(17),
                    Price = 2000,
                    TimeLimit = DateTime.Today.AddDays(7)
                }
            }
        };

        return Ok(response);
    }
}